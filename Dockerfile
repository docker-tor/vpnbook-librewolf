FROM debian:latest
MAINTAINER Timothy Redaelli <timothy.redaelli@gmail.com>

ENV DEBIAN_FRONTEND=noninteractive

# Set some needed APT options
RUN printf '%s' 'APT::Install-Recommends "true";' 'APT::Install-Suggests "false";' 'Acquire::Retires "10";' > /etc/apt/apt.conf.d/99local

RUN apt-get update -y && apt-get install -y ca-certificates gnupg iproute2 wget

RUN wget -O /usr/share/keyrings/xpra-2022.gpg https://xpra.org/xpra-2022.gpg
RUN wget -O /usr/share/keyrings/librewolf.gpg https://deb.librewolf.net/keyring.gpg
RUN wget -P /etc/apt/sources.list.d https://xpra.org/repos/$(sed -n 's/VERSION_CODENAME=//p' /usr/lib/os-release)/xpra.list
RUN printf '%s\n' \
    'Types: deb' \
    'URIs: https://deb.librewolf.net' \
    "Suites: $(sed -n 's/VERSION_CODENAME=//p' /usr/lib/os-release)" \
    'Components: main' \
    'Architectures: amd64' \
    'Signed-By: /usr/share/keyrings/librewolf.gpg' \
    > /etc/apt/sources.list.d/librewolf.sources

# Install official packages
RUN apt-get update -y && apt-get install -y \
    librewolf \
    openssh-server \
    xpra && \
# Cleanup
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -f /etc/apt/apt.conf.d/99local && \
    rm -f /etc/ssh/ssh_host_*

RUN mkdir -p /home/anon /var/run/sshd

COPY .startup.sh /usr/local/sbin/startup.sh

CMD /usr/local/sbin/startup.sh
