#!/bin/sh

set -e

# Copyright 2022, Timothy Redaelli <timothy.redaelli@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License at <http://www.gnu.org/licenses/> for
# more details.


if command -v podman >/dev/null; then
	docker='sudo podman'
else
	docker='sudo docker'
fi

bold=$(tput bold)
normal=$(tput sgr0)
ceol=$(tput el)
up=$(tput cuu1)

bold_echo() {
	echo "$bold$@$normal"
}

check_packages() {
	$docker --version >/dev/null || return
	sudo --version >/dev/null || return
	ssh -V 2>/dev/null || return
	xpra --version >/dev/null || return
}

find_docker_ip() {
	local name="$1" net="$2"
	local id='' ip='' i=0
	while [ -z "$ip" ]; do
		if [ $i -gt 10 ]; then
			bold_echo "Fatal Error" >&2
			exit 1
		fi
		i=$((i + 1))
		id=$($docker ps -q -f "name=$name" 2>/dev/null) || continue
		ip=$($docker inspect -f "{{ .NetworkSettings.Networks.$net.IPAddress }}" "$id" 2>/dev/null) || continue
		sleep 1
	done
	echo "$ip"
}

# Cleanup
cleanup() {
	local x
	bold_echo "Cleaning up..."
	$docker ps -aq -f "name=docker_vpnbook_librewolf" | while read -r x; do
		$docker kill "$x" >/dev/null || :
		$docker rm "$x" >/dev/null
	done
	$docker network rm docker_vpnbook_tor_node_internal docker_vpnbook_librewolf_internal docker_vpnbook_tor_node_external >/dev/null 2>&1 || :
	if [ -n "$keydir" ]; then
		rm -rf "$keydir"
	fi
}

if ! check_packages; then
	bold_echo "You need to install docker or podman, openssh, sudo and xpra before starting this script" >&2
	exit 1
fi

trap 'cleanup' EXIT
trap 'cleanup; trap - INT EXIT; kill -INT $$' INT
trap 'cleanup; trap - TERM EXIT; kill -TERM $$' TERM

cleanup

# Waiting for cleanup to complete
bold_echo "Waiting for cleanup to complete..."
while [ -n "$($docker ps -aq -f "name=docker_vpnbook_librewolf")" ]; do
	sleep 1
done

if [ "$1" = "build" ]; then
	bold_echo "Building docker containers..."
	# Always (to import security fixes) build images and create containers
	$docker build --pull=true -t registry.gitlab.com/docker-tor/tor-node "https://gitlab.com/docker-tor/tor-node.git"
	$docker build --pull=true -t registry.gitlab.com/docker-tor/vpnbook "https://gitlab.com/docker-tor/vpnbook.git"
	$docker build --pull=true -t registry.gitlab.com/docker-tor/vpnbook-librewolf "https://gitlab.com/docker-tor/vpnbook-librewolf.git"
else
	bold_echo "Downloading docker containers..."
	# Use pre-built images
	$docker pull registry.gitlab.com/docker-tor/tor-node
	$docker pull registry.gitlab.com/docker-tor/vpnbook
	$docker pull registry.gitlab.com/docker-tor/vpnbook-librewolf
fi

# Create networks
bold_echo "Creating docker networks..."
$docker network create --internal docker_vpnbook_tor_node_internal >/dev/null
$docker network create --internal docker_vpnbook_librewolf_internal >/dev/null
$docker network create docker_vpnbook_tor_node_external >/dev/null

# Create temporary SSH key
bold_echo "Creating SSH key"
keydir=$(mktemp -d)
ssh-keygen -q -N '' -f "$keydir"/id_rsa

# Create tor_node containers
bold_echo "Creating docker tor_node container..."
$docker create --net docker_vpnbook_tor_node_internal --name docker_vpnbook_librewolf_tor_node --env DISABLE_RINETD=true registry.gitlab.com/docker-tor/tor-node >/dev/null

# Attach networks to tor_node cointainer
$docker network connect docker_vpnbook_tor_node_external docker_vpnbook_librewolf_tor_node

# Start containers
bold_echo "Starting tor_node container..."
$docker start docker_vpnbook_librewolf_tor_node >/dev/null

bold_echo "Waiting until tor-node fully starts..."
tries=0
while :; do
	tries=$((tries + 1))
	i=0
	until $docker exec docker_vpnbook_librewolf_tor_node curl --socks5-hostname "127.0.0.1:9050" -m 5 -sI https://check.torproject.org/ >/dev/null; do
		i=$((i + 1))
		printf "\r${ceol}Try %d [%d/3]" $tries $i
		if [ $i -ge 3 ]; then
			$docker kill docker_vpnbook_librewolf_tor_node >/dev/null
			$docker start docker_vpnbook_librewolf_tor_node >/dev/null
			continue 2
		fi
	done
	break
done
echo

bold_echo "Loading tun kernel module..."
sudo modprobe tun

bold_echo "Starting vpnbook container..."
$docker create --cap-add=NET_ADMIN --device /dev/net/tun --net docker_vpnbook_librewolf_internal --name docker_vpnbook_librewolf_vpnbook --env TOR_NODE_IP="$(find_docker_ip docker_vpnbook_librewolf_tor_node docker_vpnbook_tor_node_internal)" registry.gitlab.com/docker-tor/vpnbook >/dev/null
$docker network connect docker_vpnbook_tor_node_internal docker_vpnbook_librewolf_vpnbook
$docker start docker_vpnbook_librewolf_vpnbook >/dev/null

bold_echo "Waiting until vpnbook fully starts..."
until $docker exec docker_vpnbook_librewolf_vpnbook curl --socks5-hostname "127.0.0.1:1080" -m 5 -sI https://check.torproject.org/ >/dev/null; do
	sleep 1
done

bold_echo "Starting librewolf container..."
$docker create --net docker_vpnbook_librewolf_internal --name docker_vpnbook_librewolf_librewolf --env UID="$(id -u)" --env GID="$(id -g)" --env VPNBOOK_NODE_IP="$(find_docker_ip docker_vpnbook_librewolf_vpnbook docker_vpnbook_librewolf_internal)" --volume="$keydir/id_rsa.pub:/home/anon/.ssh/authorized_keys:Z" registry.gitlab.com/docker-tor/vpnbook-librewolf >/dev/null

$docker start docker_vpnbook_librewolf_librewolf >/dev/null

bold_echo "Finding librewolf IP address..."
IP=$(find_docker_ip docker_vpnbook_librewolf_librewolf docker_vpnbook_librewolf_internal)

bold_echo "Connecting to librewolf via SSH and launching librewolf..."
xpra start --encoding=rgb --exit-with-children=yes --ssh="ssh -i $keydir/id_rsa -oUserKnownHostsFile=/dev/null -oConnectionAttempts=100 -oConnectTimeout=60 -oStrictHostKeyChecking=no -oCheckHostIP=no" ssh://"anon@$IP" --start-child="/usr/bin/librewolf"
